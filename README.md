# VSCodiumSettingsSync

Personal settings (Extension, keybindings, snippets) for VSCodium (WIP), synced using [SyncSettings](https://github.com/zokugun/vscode-sync-settings)